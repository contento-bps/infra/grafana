# Especificamos la version de grafana.
ARG GRAFANA_IMAGE_VERSION="7.3.5-ubuntu"

# Basado e la imagen Base.
FROM grafana/grafana:${GRAFANA_IMAGE_VERSION}

ARG GF_BUILD_INSTALL_PLUGINS=""

COPY build-install-plugins.sh /tmp/scripts/build-install-plugins.sh

RUN /tmp/scripts/build-install-plugins.sh "${GF_BUILD_INSTALL_PLUGINS}"

